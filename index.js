console.log("Hello!")

// [SECTION] Exponent operator

// -ES6 Update
const firtNum = 8 ** 2;
console.log(firtNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

// [SECTION] Template Literals
/*
	Allows to write strings without using th concatenation (+)
	Greatly helps with code readability
*/

let name = "John";

// Pre-template literal quote
// Uses single qu=oute or double qoute
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals:\n" + message);

// String Using template literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming!`;
console.log(`Message without template literals: \n${message}`);

const anotherMessage = `${name} attended a math competition.`
console.log(`${anotherMessage}\nHe won it by solving the problem 8 ** 2 with the solution of ${firtNum}`);

// answer to mini activity
const anotherMessage2 = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firtNum}.`
console.log(anotherMessage2);

/*
	-Template literals allows us to write strings with embedded javascript expressions
	-Expressions are any valid unit of code that resolves to a value
	-"${}" are used to include javascript expressions in strings using template literals
*/

const interestRate = .1;
const principal = 100;

console.log(`The interest if your savings account is ${principal*interestRate}`);

// [SECTION] Array Destructuring
/*
	-Allows us to unpack elements in array into distinct variables
	-Allows us to name array elements with variables, instead of using index numbers
	-Helps with code readability
	Syntax
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// my answer
let message1 = `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you`
console.log(message1)
// Answer to mini activity 2
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you`)


// Array Destructuring // -ES6 Update

const [firstName, middleName, lastName] = fullName
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`)

// [SECTION] Object Destructuring 
/*
	-Allows us to unpack properties/keys of objects into distinct variables
	-Shortens the syntax for accessing propersties from objects
	-Syntax
		let/const{propertyVariableName, propertyVariableName, properttVariableName} = object

	-Note - Rule:
		The variableName to be assigned in destructuring should be the same as the propertyName


*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz",
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again`)


// Object Destructuring // ES6 Update
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`)

// [SECTION] Arrow function
/*
	-Compact alternative syntax to traditional functions
	-Useful for code snippets where creating function will not be reused in any other portion of the code
	-Adheres to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name of function that will only be used in certain code snippets.

	-Syntax
		const variableName = () => {
			console.log("");
		}
*/

const hello = () => {
	console.log("Hello World!");
}
hello();

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("John", "D.","Smith");

const studetns = ["John", "Jane", "Judy"];

console.log("===============");
// Arrow Functions with loops
// Pre-arrow functions

students.forEach(function(student){
	console.log(`${student} is a student.`);
})

// Arrow Function is forEach // ES6 Update
// The function is only used in the "forEach" method to pring out a text woith the student's names

students.forEach((student) => {
	console.log(`${student} is a student.`);


})

console.log("===============");

// [SECTION] Implicit Return Statement
/*
	-There are instances when you can omit the 'return' statement
	-This works because without the 'return' statement JavaScript implicitly adds it for the result of the function


*/

// Pre-Arrow Function

/*const add = (numA, numB) => {
	return numA + numB;
}
let total = add(1,2);
console.log(total);*/


const add = (numA, numB) => numA + numB
let total = add(1,2)
console.log(total);

// [SECTION] Default function argument value
//  Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good morning, ${name}`
}

console.log(greet("John!"));
console.log(greet());

// [SECTION] Class-based object Blueprints
/*
	-It allows creation/instantation of objects using classes as blueprints
	Syntax
		class className{
			constructor(objectPropertyA, objectPropertyB)
				this.objectPropertyA = objectPropertyB
				this.objectPropertyB = objectPropertyB
		} 
}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Intantiating an object
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Range Raptor";
myCar.year = 2021;
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);
